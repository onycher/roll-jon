
var RollJon = RollJon || (function () {
    'use strict';

    function getLeft(matrix, i, j) {
        if ( j <= 0 ) {
            return 'W'
        }
        return matrix[i][j-1]
    }

    function getRight(matrix, i, j) {
        if (j >= matrix[i].length-1) {
            return 'W'
        }
        return matrix[i][j+1]
    }

    function getTop(matrix, i, j) {
        if (i <= 0) {
            return 'W'
        }
        return matrix[i-1][j]
    }
    function getBottom(matrix, i, j) {
        if (i >= matrix.length-1) {
            return 'W'
        }
        return matrix[i+1][j]
    }

    function makeMatrix(raw_data) {
        raw_data = raw_data.trim().split('S')
        var matrix = []
        for(let i = 0; i < raw_data.length; i++) {
            matrix.push(Array.from(raw_data[i]));
        }
        return matrix
    }

    function findEdges(matrix) {
        let edges = []
        for(let i = 0; i < matrix.length; i++) {
            for(let j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] === 'W') {
                    if (getLeft(matrix, i, j) !== 'W') {
                        edges.push([[i,j], [i+1,j]])
                    }
                    if (getRight(matrix, i, j) !== 'W') {
                        edges.push([[i,j+1], [i+1, j+1]])
                    }
                    if (getTop(matrix, i, j) !== 'W') {
                        edges.push([[i,j], [i,j+1]])
                    }
                    if (getBottom(matrix, i, j) !== 'W') {
                        edges.push([[i+1,j],[i+1,j+1]])
                    }
                }
            }
        }
        return edges
    }

    function createWalls(edges) {
        for(let i = 0; i < edges.length; i++) {
            createObj('path', {
                pageid: Campaign().get('playerpageid'),
                path: "{[\"M\", "+edges[i][0][1]*140+", "+edges[i][0][0]*140+"], [\"L\", "+edges[i][1][1]*140+", "+edges[i][1][0]*140+"]}",
                layer: 'walls',
                stroke: "#FFFF00",
                stroke_width: 3
            })
            
        }
    }

    function convert_data() {
        let rows = RollJonData.split('\n')
            let array = []
            for (let i = 0; i < rows.length; i++) {
                array.push(rows[i].split('\t'))
            }
            let converted = []
            for (let i = 0; i < array.length; i++) {
                 if (array[i].length < 2) {
                    continue
                }
                log(array[i].length)
                for (let j = 0; j < array[i].length; j++) {
                    if (array[i][j] === 'F') {
                        converted.push('F')
                    }
                    else if (array[i][j] === '') {
                        converted.push('W')
                    }
                    else {
                        converted.push('D')
                    }
                }
                converted.push('S')
            }
            return converted.join('')
    }

    function makeWalls() {
        let data = convert_data()
        let matrix = makeMatrix(data)
        let edges = findEdges(matrix)
        createWalls(edges) 
    }

    function handleInput(msg_orig) {
        let msg = _.clone(msg_orig);
        if (msg.type !== "api") {
            return;
        }
        if (msg.content === '!RollJon') {
            makeWalls()
        }
    };


    function registerEventHandlers() {
        on('chat:message', handleInput);
    };

    return {
        RegisterEventHandlers: registerEventHandlers,
        makeWalls: makeWalls
    };
}());

on("ready", function () {
    'use strict';
    RollJon.RegisterEventHandlers();
});

