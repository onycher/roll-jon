import sys

def convert(data):
    data = data.split('\n')
    array = [row.split('\t') for row in data]
    converted = []
    for row in array:
        for space in row:
            if space == 'F':
                converted.append('F')
            elif space == '':
                converted.append('W')
            else:
                converted.append('D')
        converted.append('S')
    return ''.join(converted[:-1])

def main():
    if len(sys.argv) != 3:
        print('Usage: {} in-file out-file'.format(sys.argv[0]))
    in_file = sys.argv[1]
    out_file = sys.argv[2]
    with open(in_file, 'r') as f:
        data = f.read()
    converted = convert(data)
    with open(out_file, 'w') as f:
        f.write(converted)
if __name__ == '__main__':
    main()